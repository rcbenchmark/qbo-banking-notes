//$('body').append("QBO BANKING NOTES EXTENSION ACTIVE");

// todo: stop polling and suggest a refresh after 5 minutes

var BNOTES = {
  cachedRows: {},
  dbkey: null,
  error: false
};

function init(){
  // check if user configured a key or not
  chrome.storage.sync.get("bnotekey", function(key){
    if(isEmpty(key)){
      newKey = prompt("QBO banking notes extension:\n\nTo share notes with other users, please all use the same password and type it here. Once a password is set, you need to reinstall the extension if you want to use a different one.");
      if(newKey){
        chrome.storage.sync.set({ "bnotekey": newKey }, function(){
            BNOTES.dbkey = newKey;
            startPolling();
        });
      }
    }else{
      BNOTES.dbkey = key.bnotekey;
      console.log("Bank notes extension key is: " + BNOTES.dbkey);
      startPolling();
    }
  });
}

function rowIdToDatabaseKey(rowid){
  return rowid.replace(/[^A-Za-z0-9]/g, '');
}

function getRowComment(rowid, callback){
  getDatabaseVal(rowIdToDatabaseKey(rowid), callback);
}

function saveNewNote(inputid, note){
  $('#' + inputid + ' > .save-status').text("Saving ...");
  var itemkey = rowIdToDatabaseKey(inputid);
  updateDatabaseVal(itemkey, note, function(newTime){
    $('#' + inputid + ' > .save-status').text("");
    $('#' + inputid + ' > .save-time').text(new Date(newTime).toString());
  });
}

function renderRowComment(rowid, inputid, comment, time){
  if($('#' + inputid).length === 0){
    // add the element
    var timestr = "";
    if(time)
      timestr = new Date(time).toString();
    var html = '<div id="' + inputid + '" class="bnotes-form"><input type="text" name="banknote" value="' + comment + '" placeholder="Bookkeeping comment"><strong class="save-status" style="color=red;"></strong><small class="save-time">' + timestr + '</small></div>';
    document.getElementById(rowid).insertAdjacentHTML('beforeend', html);
    
    // register its edit callback
    $('#' + inputid + ' > input').blur(function(){
      var newVal = $(this).val();
      var oldVal = BNOTES.cachedRows[rowIdToDatabaseKey(inputid)];
      if(oldVal === undefined)
        oldVal = newVal;
      if(newVal.trim() !== oldVal.trim())
        saveNewNote(inputid, newVal);
    });
  }
}

// QBO works as a single page application, so we must poll as its hard to detect page changes.
function startPolling(){
  setInterval(function(){
    if(BNOTES.error){
      $(".bnotes-form").hide();
      return;
    }
    
    if(location.pathname !== "/app/banking")
      return;
    
    // update the list of visible rows
    var visibleRows = [];
    $( "div[role='row']" ).each(function(){
      if(this.id && this.id.length > 0)
        visibleRows.push(this.id);
    });

    // process those rows
    visibleRows.forEach(function(rowid){
      if(BNOTES.error)
      return;
      
      var inputid = rowid.split(':')[0];
      
      // Add note under row
      getRowComment(inputid, function(comment, time){
        renderRowComment(rowid, inputid, comment, time);
      });
    });
  },1000);
}

function isEmpty(obj) {
  return Object.getOwnPropertyNames(obj).length === 0;
}

function getDatabaseVal(itemkey, callback) {
    // check the cache first
    if(BNOTES.cachedRows[itemkey] !== undefined){
      //console.log("Getting from cache: " + itemkey);
      callback(BNOTES.cachedRows[itemkey], BNOTES.cachedRows[itemkey + "t"]);
      return;
    }
    //console.log("Getting from DB: " + itemkey);
    $.ajax({
        type: "GET",
        url: "https://keyvalue.immanuel.co/api/KeyVal/GetValue/" + BNOTES.dbkey + "/" + itemkey,
        contentType: false,
        processData: false
      }).done(function (data) {
        try{
          data = window.atob(decodeURIComponent(data)).trim();
        }catch (err) {
          alert(err);
          return;
        }
      
        $.ajax({
            type: "GET",
            url: "https://keyvalue.immanuel.co/api/KeyVal/GetValue/" + BNOTES.dbkey + "/" + itemkey + "t",
            contentType: false,
            processData: false
          }).done(function (time) {
            time = Number(time);
            // update cache
            BNOTES.cachedRows[itemkey + "t"] = time;
            BNOTES.cachedRows[itemkey] = data;
            //console.log(time);
            callback(data, time);
          }).fail(function(err){
            console.log("Bank notes extention: Error reaching database");
            console.log(err);
            BNOTES.cachedRows[itemkey] = '';
            callback('');
        });
      }).fail(function(err){
        console.log("Bank notes extention: Error reaching database");
        console.log(err);
      
        // database has an issue that null values cause it to crash, to we autorepair
        if(err.status === 500 && err.responseJSON && err.responseJSON.ExceptionType === "System.InvalidCastException"){
          console.log("Reparing database null value...");
          updateDatabaseVal(itemkey, '', function(){
            console.log("repaired");
            BNOTES.cachedRows[itemkey] = '';
            callback('');
          });
        }
    });
}

function updateDatabaseVal(itemkey, itemval, callback) {
    if(!itemval || itemval.length === 0)
      itemval = " ";
    var encVal;
    try{
      encVal = encodeURIComponent(window.btoa(itemval));
    }catch (err){
      alert(err);
      return;
    }
    if(encVal.length > 1000){
      alert("Error: note cannot be saved as it is too long.");
      return;
    }
    $.ajax({
        type: "POST",
        url: "https://keyvalue.immanuel.co/api/KeyVal/UpdateValue/" + BNOTES.dbkey + "/" + itemkey + "/" + encVal,
        contentType: false,
        processData: false
       }).done(function (data) {
          // save the timestamp
          var time = Date.now();
          $.ajax({
              type: "POST",
              url: "https://keyvalue.immanuel.co/api/KeyVal/UpdateValue/" + BNOTES.dbkey + "/" + itemkey + "t/" + time,
              contentType: false,
              processData: false
             }).done(function (data) {
                // update cache
                BNOTES.cachedRows[itemkey + "t"] = time;
                BNOTES.cachedRows[itemkey] = itemval;
                callback(time);
             }).fail(function(err){
              alert("Bank notes extention: Error reaching database");
              BNOTES.error = true;
          });
       }).fail(function(err){
        alert("Bank notes extention: Error reaching database");
        BNOTES.error = true;
    });
}

init();